#!/bin/bash
# Input arguments:
inputarg=$1

echo "Enter name of course (fluid | regtek | objprog | elmag)"
read coursename

until [ "$coursename" == "fluid" ] ||
    [ "$coursename" == "regtek" ] ||
    [ "$coursename" == "objprog" ] || 
[ "$coursename" == "elmag" ] 
do
    echo "Enter name of course (fluid | regtek | objprog | elmag)"
    read coursename
done

if [ "$inputarg" == "--forelesning" ] 2>/dev/null ; then
    DATE=$(date +"%d-%m-%Y")
    proj=/home/$USER/NTNU/"$coursename"/forelesninger/"$DATE"
    if [[ ! -d "$proj" && ! -L "$proj" ]] ; then
        # Create directory for new oving.
        mkdir -p "$proj"
        echo "Directory created for forelesning ..."
        # Copy in files
        cp -i /home/$USER/NTNU/template/main.tex                 $proj   
        cp -i /home/$USER/NTNU/template/exerciseSettings.tex     $proj
        cp -i /home/$USER/NTNU/template/exerciseBody.tex         $proj

        echo "\\toggletrue{printDate}" >> $proj/exerciseSettings.tex
        echo "\\toggletrue{$coursename}" >> $proj/exerciseSettings.tex
        echo "\\newcommand{\\creationDate}{$DATE}" >> $proj/exerciseSettings.tex

        # Open files
        #urxvt -e latexmk -jobname="$coursename$date" $proj/main.tex &
        urxvt -e latexmk $proj/main.tex &
        vim $proj/exerciseBody.tex
    else
        echo "The directory already exists. Opening files in vim..."
        urxvt -e latexmk -quiet -jobname="$coursename$date" $proj/main.tex &
        vim $proj/exerciseBody.tex
    fi
    exit 1
fi

echo "Enter number of exercise (XX) ..."

read exerciseNumber

varLength=${#exerciseNumber}
until [ $varLength == 2 ] && [ $exerciseNumber -eq $exerciseNumber ] 2>/dev/null
do
    echo "Try again ..."
    echo "Input number (XX): "
    read exerciseNumber
    varLength=${#exerciseNumber}
done

dirname="ov$exerciseNumber"
proj=/home/$USER/NTNU/"$coursename"/"$dirname"



if [[ ! -d "$proj" && ! -L "$proj" ]] ; then
    # Create directory for new oving.
    mkdir -p "$proj"
    echo "Directory created ..."

    # Copy files into new oving. Should have an extra check.
    cp -i /home/$USER/NTNU/template/main.tex                 $proj   
    cp -i /home/$USER/NTNU/template/exerciseSettings.tex     $proj
    cp -i /home/$USER/NTNU/template/exerciseBody.tex         $proj

    echo "Files created ..."

    # Ask for figures here
    if [ "$inputarg" == "--fig" ] 2>/dev/null ; then
        COUNTER=1
        echo "Do you want to add figures to project folder? (y/n)"
        read answer
        if [ "$answer" == "y" ] ; then
            mkdir -p "$proj/figures"
            scrot -s "fig$COUNTER.png" -e "mv fig$COUNTER.png $proj/figures"
            echo "\\pic{figures/fig$COUNTER}{0.5}{}" >> $proj/exerciseBody.tex
        fi

        until [ "$answer" == "n" ]
        do
            # Take screenshot of area, then add to project folder.
            # Also include it in latex.
            echo "One more?"
            read answer
            if [ "$answer" == "y" ] ; then
                let COUNTER=COUNTER+1 
                scrot -s "fig$COUNTER.png" -e "mv fig$COUNTER.png $proj/figures"
                echo "\\section*{Oppgave $COUNTER}" >>  $proj/exerciseBody.tex
                echo "\\pic{figures/fig$COUNTER}{0.5}{}" >> $proj/exerciseBody.tex
            fi
        done
    fi

    # Add things that should be automated
    exerciseNumber=$(echo $dirname | cut -d'v' -f 2)
    echo "\\newcommand{\\whichExercise}{ØVING $exerciseNumber }" >> $proj/exerciseSettings.tex
    echo "\\toggletrue{$coursename}" >> $proj/exerciseSettings.tex
    echo "\\toggletrue{printFeedback}" >> $proj/exerciseSettings.tex
    echo "\\toggletrue{printExercise}" >> $proj/exerciseSettings.tex
    vim $proj/exerciseSettings.tex
    urxvt -e latexmk -quiet -jobname="$coursename$exerciseNumber" $proj/main.tex &
    vim $proj/exerciseBody.tex
else
    # Ask for figures here too
    if [ "$inputarg" == "--fig" ] 2>/dev/null ; then
        COUNTER=1
        echo "Do you want to add figures to project folder? (y/n)"
        read answer
        if [ "$answer" == "y" ] ; then
            mkdir -p "$proj/figures"
            scrot -s "fig$COUNTER.png" -e "mv fig$COUNTER.png $proj/figures"
            echo "\\pic{figures/fig$COUNTER}{0.5}{}" >> $proj/exerciseBody.tex
        fi

        until [ "$answer" == "n" ]
        do
            # Take screenshot of area, then add to project folder.
            # Also include it in latex.
            echo "One more?"
            read answer
            if [ "$answer" == "y" ] ; then
                let COUNTER=COUNTER+1 
                scrot -s "fig$COUNTER.png" -e "mv fig$COUNTER.png $proj/figures"
                echo "\\section*{Oppgave $COUNTER}" >>  $proj/exerciseBody.tex
                echo "\\pic{figures/fig$COUNTER}{0.5}{}" >> $proj/exerciseBody.tex
            fi
        done
    fi
    echo "The directory already exists. Opening files in vim..."
    urxvt -e latexmk -quiet -jobname="$coursename$exerciseNumber" $proj/main.tex &
    vim $proj/exerciseBody.tex
fi
